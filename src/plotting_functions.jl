#plot_funcs = (:myscatter,:myplot, :mybar)

function myscatter(x,y,colore,option, label; d = 0)
  s = scatter(; x = x,
                y = y,
                mode = "markers",
                error_y = Dict(:type => "data", :array => d, :visible => true, :color => colore),
                marker=attr(color= option? colore : colorant"white",
                            line_color = colore,
                            line_width = 3,
                            size=12,
                            symbol="circle"),
                name = label)
  return [s]
end

function mybar(x,y,colore,option, label; d = 0)
  s = bar(; x = x,
            y = y,
            mode = "markers",
            error_y = Dict(:type => "data", :array => d, :visible => true, :color => colore),
            marker=attr(color= option? colore : colorant"white",
                        line_color = colore,
                        line_width = 3,
                        size=12),
            name = label)
  return [s]
end

function myplot(x,y,colore, option,label; d = 0)
    s1 = scatter(;  x = x,
                    y = y,
                    mode = "lines",
                    line_dash = option? "solid":"dash",
                    marker_color = colore,
                    name = label)
    xline, yline = contorno(x,y,d)
    alpha = 0.2
    s2 = scatter(;  x = xline,
                    y = yline,
                    line_width = 0,
                    mode = "lines",
                    fill = "tozeroy",
                    showlegend = false,
                    fillcolor = lightershade(colore,alpha),
                    name = "")
    return [s1,s2]
end

function contorno(x,y,d)
    yline = reduce(vcat,[y-d,(y+d)[end:-1:1],(y+d)[1]])
    xline = reduce(vcat,[x,x[end:-1:1],x[1]])
    return xline, yline
end

function lightershade(oldcolor,alpha)
  return RGBA(getfield(oldcolor,:r),getfield(oldcolor,:g),getfield(oldcolor,:b), alpha)
end









# function myplot!(figura, x,y,d;kwargs...)
#     palette = get_color_palette(:auto, default(:bgcolor),100)
#     plot!(figura,x,y, color=palette[figura.n%100+1];kwargs...)
#     figura.n -= 1
#     plot!(figura,contorno(x,y,d)...,line = 0, alpha = 0.4, fill=(0,palette[figura.n%100+1]),
#     label = "")
# end
#
# function myscatter!(figura, x,y,d;kwargs...)
#     palette = get_color_palette(:auto, default(:bgcolor),100)
#     scatter!(figura,x,y, err=d, color=palette[figura.n%100+1];kwargs...)
# end
