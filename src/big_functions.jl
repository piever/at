# import analysis functions and their names (vector funcs)

include("analysis_functions.jl")

# import plotting functions and their names (vector plot_funcs) $(parse("$(func)m"))

include("plotting_functions.jl")


function getxy_(df,xaxis,x,y, mouse_error)
  mean_across_mice = DataArray(Float64, length(xaxis));
  sem_across_mice = DataArray(Float64, length(xaxis));
  valid_axis = Array(Bool, length(xaxis));
  if mouse_error
    splitdata = groupby(df, :MouseID)
    v = DataArray(Float64, length(xaxis), length(splitdata));
    for i in 1:length(splitdata)
      v[:,i] = getxy(splitdata[i],xaxis,x,y)
    end
    for j in 1:length(xaxis)
      mean_across_mice[j] = mean(dropna(v[j,:]))
      sem_across_mice[j] = sem(dropna(v[j,:]))
      valid_axis[j] = (length(dropna(v[j,:]))> 1)
    end
  else
    mean_across_mice = getxy(df,xaxis,x,y)
    sem_across_mice[:] = 0
    valid_axis = ~isna(mean_across_mice)
  end
  return mean_across_mice, sem_across_mice, valid_axis
end

#PlotlyJS version
function getxy_plot(colori, df, zv, x, y, plot_func; nbins = 0)
  # get xaxis
  df[:fake] = [0 for i in 1:size(df,1)]
  new_x = x
  if nbins > 0
    binned = linspace(minimum(df[x]),maximum(df[x]),nbins+1)
    df[:binned] = DataArray(Float64,size(df,1))
    binsize = (binned[2]-binned[1])
    for val in binned
      indexes = (val .<= df[x] .< val+binsize)
      df[indexes,:binned] = val+binsize/2
    end
    new_x = :binned
  end
  xaxis = union(df[new_x])
  sort!(xaxis)
  # initialize figure
  traces = Array(PlotlyJS.GenericTrace{Dict{Symbol,Any}},0)
  indice = [0,0]
  num_cond = length(union(df[zv[end]]))
  # fill figure!
  by(df,zv) do dd
    colore = colori[indice[2]%length(colori)+1]
    option = (indice[1]%2 == 0)
    indice[2] = indice[2]+1
    if indice[2] >= num_cond
      indice[2] = 0
      indice[1] = indice[1]+1
    end
    mouse_error = ~any(vcat(zv,[x,y]).== :MouseID)
    media, errore, validi = getxy_(dd,xaxis,new_x,y, mouse_error)
    zvr = filter(t -> t != :fake, zv)
    if isempty(zvr)
      label = ""
    else
      label = mapreduce(t-> "$t = $(dd[1,t]) ",string,"",zvr)
    end
    s = plot_func(xaxis[validi],media[validi], colore, option, label;
    d = errore[validi])
    append!(traces,s)
    return
  end
  return traces
end


function getpermousesplit(colori, df, zv, funcx, funcy;kwargs...)
  df[:fake] = [0 for i in 1:size(df,1)]
  # fill figure!
  traces = Array(PlotlyJS.GenericTrace{Dict{Symbol,Any}},0)
  indice = [0,0]
  num_cond = length(union(df[zv[end]]))
  # fill figure!

  by(df,zv) do dd
    colore = colori[indice[2]%length(colori)+1]
    option = (indice[1]%2 == 0)
    indice[2] = indice[2]+1
    if indice[2] >= num_cond
      indice[2] = 0
      indice[1] = indice[1]+1
    end
    xdots, ydots = getpermouse(dd,funcx, funcy)
    label = mapreduce(t-> "$t = $(dd[1,t]) ",string,"",zv)
    s = myscatter(xdots, ydots, colore,option,label)
    append!(traces,s)
    return
  end
  return traces
end
