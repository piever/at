# Prima di tutto definisci la funzione che vuoi applicare

#funcs = (:getxy, :gethist, :getcum, :gethazard)

function getxy(df, xaxis, x, y::Symbol)
  media = by(df, x) do dd
      DataFrame(m = mean(dd[y]))
  end
  aux = DataFrame()
  aux[x] = xaxis
  mediaoverx = join(aux, media, on = x, kind = :left)
  sort!(mediaoverx, cols = [x])
  return mediaoverx[:m]
end

function getxy(df, xaxis, x, f::Function)
  return f(df, xaxis, x)
end

function gethist(df, xaxis, x)
  vect = [length(find(df[x] .== t)) for t in xaxis]/length(df[x])
  return vect
end

function getcum(df, xaxis, x)
  vect = gethist(df, xaxis, x)
  return cumsum(vect)
end

function gethazard(df, xaxis, x)
  vect = 1-getcum(df, xaxis, x)
  vect2 = DataArray(Float64,length(xaxis))
  vect2[1] = 1-vect[1]
  vect2[2:end] = -diff(vect)./vect[1:end-1]
  vect2[vect.== 0] = 1
  return vect2
end

function getpermouse(df,funcx, funcy)
  dati_condition = by(df,:MouseID) do dd
    DataFrame(xdots = funcx(dd), ydots = funcy(dd))
  end
  return dati_condition[:xdots], dati_condition[:ydots]
end
