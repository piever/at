# Repository for analysis and plotting of a dataframe #

### What is this repository for? ###

It exports one function to select data, one to make plots at the population level: getxy_plot. First argument is colorscheme, second is dataframe, then variables along which to split, then x axis, then y axis (can be a fct), then plotting method.
Now it has the possibility of checking things also at the single mouse level.

The error bar is standard error of the mean across mice. It is assumed that the mouse label is :MouseID.

This packages relies on [DataFrames](https://github.com/JuliaStats/DataFrames.jl) and [PlotlyJS](https://github.com/spencerlyon2/PlotlyJS.jl).